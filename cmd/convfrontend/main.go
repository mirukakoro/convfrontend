package main

import (
	"fmt"
	"log"
	"os"
	"runtime/debug"

	"fyne.io/fyne/v2"

	"fyne.io/fyne/v2/container"

	app2 "fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/colourdelete/convfrontend/pkg/consts"
	"gitlab.com/colourdelete/convfrontend/pkg/ui"
	"golang.org/x/xerrors"
)

func main() {
	app := app2.NewWithID(consts.AppID)
	app.Preferences().SetString(consts.PrefSessionAPIPassword, os.Getenv("CONVFRONTEND_PASSWORD"))
	app.Preferences().SetString(consts.PrefSessionAPIClientTimeout, "5s")

	win := app.NewWindow("AIbou Client")
	defer func() {
		stack := debug.Stack()
		if r := recover(); r != nil {
			d := dialog.NewCustom("Panic!", "Close",
				container.NewVBox(
					widget.NewLabel(fmt.Sprintf("I recovered from a panic. The panic was: %s", r)),
					widget.NewLabelWithStyle(
						fmt.Sprintf("Panic:\n%#v", r),
						fyne.TextAlignLeading,
						fyne.TextStyle{Monospace: true},
					),
					widget.NewLabelWithStyle(
						fmt.Sprintf("Stack Trace:\n%s", stack),
						fyne.TextAlignLeading,
						fyne.TextStyle{Monospace: true},
					),
				), win,
			)
			d.SetOnClosed(func() {
				log.Fatalf("Panic!\n%s\n%#v\n%s", r, r, stack)
			})
			d.Show()
			app.Run()
			log.Fatalf("Panic!\n%s\n%#v\n%s", r, r, stack)
		}
	}()
	s, err := ui.NewSessions(
		app,
		win,
	)
	if err != nil {
		win.SetContent(widget.NewLabel("Failed to load."))
		d := dialog.NewError(xerrors.Errorf("Something catastrophic happened! This app will now close. Error is: %w", err), win)
		d.SetDismissText("Close")
		d.SetOnClosed(func() {
			log.Fatalf("Exiting due to user closing app due to error: %s", err)
		})
		d.Show()
	} else {
		win.SetContent(s.Cont)
		win.SetMainMenu(s.MainMenu)
	}
	win.ShowAndRun()
}
