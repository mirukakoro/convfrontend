# `audio.flac` Attribution

From [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:PDP-UY_-_Adelina_Morelli,_soprano_-_Orquesta_Estable_del_Teatro_Colon_-_Himno_Nacional_Uruguayo_-_instrumental_-_Debali_-_Figueroa_-_Victor-79694b-b79694b.flac):
> See page for author, Public domain, via Wikimedia Commons

Original filename:
```
PDP-UY_-_Adelina_Morelli,_soprano_-_Orquesta_Estable_del_Teatro_Colon_-_Himno_Nacional_Uruguayo_-_instrumental_-_Debali_-_Figueroa_-_Victor-79694b-b79694b.flac
```
