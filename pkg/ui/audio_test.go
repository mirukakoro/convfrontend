package ui

import (
	"bytes"
	_ "embed"
	"testing"
)

//go:embed audio.flac
var file []byte

func TestPlayAudio(t *testing.T) {
	if len(file) == 0 {
		t.Fatal("go:embed directive not working")
	}
	buf := bytes.NewBuffer(file)
	done, err := PlayAudio(buf)
	if err != nil {
		t.Errorf("PlayAudio failed: %s", err)
		return
	}
	<-done
}
