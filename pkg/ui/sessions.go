package ui

import (
	"errors"
	"log"
	"net/url"
	"runtime/debug"
	"strconv"
	"time"
	"unicode/utf8"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/colourdelete/convfrontend/pkg/backend"
	"gitlab.com/colourdelete/convfrontend/pkg/consts"
	"golang.org/x/xerrors"
)

type Sessions struct {
	app      fyne.App
	win      fyne.Window
	sessions []*Session
	tabs     *container.AppTabs
	session  *Session

	toolbar *widget.Toolbar

	Base backend.Base

	MainMenu     *fyne.MainMenu
	sessionsMenu *fyne.Menu

	Cont *fyne.Container
}

func NewSessions(app fyne.App, win fyne.Window) (*Sessions, error) {
	base, err := backend.NewBaseFromApp(app)
	if err != nil {
		return nil, err
	}
	s := &Sessions{
		app:      app,
		win:      win,
		Base:     base,
		sessions: make([]*Session, 0),
		session:  NewBlankSession(app, win, "Select or create a session."),
	}

	s.sessionsMenu = fyne.NewMenu("Sessions",
		fyne.NewMenuItem("New", s.ShowNewSessionDialog),
		fyne.NewMenuItemSeparator(),
		fyne.NewMenuItem("Sessions Preview Coming Soon", func() {}),
	)

	s.MainMenu = fyne.NewMainMenu(
		s.sessionsMenu,
		fyne.NewMenu("Settings",
			fyne.NewMenuItem("General", s.ShowSettingsDialog),
			fyne.NewMenuItem("TODO: split to General, Network & AI", func() {}),
		),
		fyne.NewMenu("Help",
			fyne.NewMenuItem("About", s.ShowAboutDialog),
		),
	)

	s.toolbar = widget.NewToolbar(
		widget.NewToolbarAction(theme.InfoIcon(), s.ShowAboutDialog),
		widget.NewToolbarAction(theme.ContentAddIcon(), s.ShowNewSessionDialog),
		widget.NewToolbarAction(theme.SettingsIcon(), s.ShowSettingsDialog),
	)
	s.tabs = container.NewAppTabs()
	s.Cont = container.NewMax(layout.NewSpacer(), container.NewBorder(
		s.toolbar,
		nil,
		nil,
		nil,
		s.tabs,
	))
	return s, nil
}

func (s *Sessions) NewSession(scenarioID int) error {
	s2, err := NewSession(s.app, s.win, scenarioID)
	if err != nil {
		return err
	}
	tabItem := container.NewTabItemWithIcon(
		s2.Conv.Scenario().Title,
		theme.DocumentIcon(),
		s2.Content,
	)
	s.tabs.Append(tabItem)
	s.tabs.SelectTab(tabItem)
	return nil
}

func utf8Validator(s string) error {
	if !utf8.ValidString(s) {
		return errors.New("userInput not valid UTF-8 sequence")
	}
	return nil
}

func (s *Sessions) ShowSettingsDialog() {
	nameEntry := widget.NewEntry()
	nameEntry.Validator = utf8Validator
	nameEntry.SetText(s.app.Preferences().StringWithFallback(consts.PrefSessionName, ""))
	passEntry := widget.NewEntry()
	passEntry.Validator = utf8Validator
	passEntry.SetText(s.app.Preferences().StringWithFallback(consts.PrefSessionAPIPassword, ""))
	timeoutEntry := widget.NewEntry()
	timeoutEntry.Validator = func(s string) error {
		_, err := time.ParseDuration(s)
		return err
	}
	timeoutEntry.SetText(s.app.Preferences().StringWithFallback(consts.PrefSessionAPIClientTimeout, ""))
	d := dialog.NewForm(
		"Settings",
		"OK",
		"Cancel",
		[]*widget.FormItem{
			widget.NewFormItem("Name", nameEntry),
			widget.NewFormItem("Password", passEntry),
			widget.NewFormItem("Timeout", timeoutEntry),
		},
		func(b bool) {
			if b {
				s.app.Preferences().SetString(consts.PrefSessionName, nameEntry.Text)
				s.app.Preferences().SetString(consts.PrefSessionAPIPassword, passEntry.Text)
				s.app.Preferences().SetString(consts.PrefSessionAPIClientTimeout, timeoutEntry.Text)
			}
		},
		s.win,
	)
	d.Show()
}

func (s *Sessions) ShowAboutDialog() {
	d := dialog.NewCustom(
		"About",
		"Close",
		container.NewVBox(
			widget.NewLabelWithStyle(
				"AIbou Client",
				fyne.TextAlignCenter,
				fyne.TextStyle{Bold: true},
			),
			widget.NewLabelWithStyle(
				"aka convfrontend",
				fyne.TextAlignCenter,
				fyne.TextStyle{},
			),
			widget.NewLabelWithStyle(
				"By Ken Shibata",
				fyne.TextAlignCenter,
				fyne.TextStyle{},
			),
			widget.NewHyperlinkWithStyle(
				"GitLab Repository",
				&url.URL{
					Scheme:  "https",
					Host:    "gitlab.com",
					Path:    "/colourdelete/convfrontend",
					RawPath: "/colourdelete/convfrontend",
				},
				fyne.TextAlignLeading,
				fyne.TextStyle{Italic: true},
			),
		),
		s.win,
	)
	d.Show()
}

func (s *Sessions) ShowNewSessionDialog() {
	var d dialog.Dialog
	nameEntry := widget.NewEntry()
	nameEntry.Validator = func(text string) error {
		if utf8.ValidString(text) {
			return nil
		} else {
			return errors.New("invalid UTF-8 sequence")
		}
	}
	scenarioIDEntry := widget.NewEntry()
	scenarioIDEntry.Validator = func(text string) error {
		_, err := strconv.ParseUint(text, 10, 16)
		// uint since negatives disallowed anyway
		// 16 bitsize since uint, not int
		return err
	}
	d = dialog.NewForm(
		"Create Session",
		"Create",
		"Cancel",
		[]*widget.FormItem{
			widget.NewFormItem(
				"Scenario ID",
				scenarioIDEntry,
			),
			//widget.NewFormItem(
			//	"Name",
			//	nameEntry,
			//),
		},
		func(b bool) {
			if b {
				bar := widget.NewProgressBarInfinite()
				d := dialog.NewCustom(
					"Creating Session",
					"Background",
					bar,
					s.win,
				)
				d.Show()

				scenarioID, err := strconv.ParseInt(scenarioIDEntry.Text, 10, 32)
				if err != nil {
					d.Hide()
					s.Complain(err)
					return
				}
				err = s.NewSession(int(scenarioID))
				if err != nil {
					d.Hide()
					s.Complain(err)
					return
				}
				d.Hide()
			}
		},
		s.win,
	)
	d.Show()
}

func (s *Sessions) ComplainIf(err error) {
	if unwrappedErr := xerrors.Unwrap(err); unwrappedErr != nil {
		s.Complain(unwrappedErr)
	}
	if err != nil {
		s.Complain(err)
	}
}

func (s *Sessions) Complain(err error) {
	stack := debug.Stack()
	fyne.LogError("complained", err)
	log.Println(string(stack))
	dialog.NewError(err, s.win).Show()
}
