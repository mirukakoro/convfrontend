package ui

import (
	"fmt"
	"log"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/colourdelete/convfrontend/pkg/backend"
	"gitlab.com/colourdelete/convfrontend/pkg/consts"
	"gitlab.com/colourdelete/convfrontend/pkg/types"
	"golang.org/x/xerrors"
)

type Session struct {
	Content       *fyne.Container
	MsgsCont      *fyne.Container
	MsgEntry      *widget.Entry
	MsgSendBtn    *widget.Button
	MsgsReloadBtn *widget.Button

	Msgs []fyne.CanvasObject

	Base backend.Base
	Conv *backend.Conv

	app fyne.App
	win fyne.Window
}

type aligner func(object fyne.CanvasObject) *fyne.Container

func alignLeft(obj fyne.CanvasObject) *fyne.Container {
	return container.NewHBox(
		obj,
		layout.NewSpacer(),
	)
}

// Named alignMiddle instead of alignCentre/alignCenter to avoid confusion.
func alignMiddle(obj fyne.CanvasObject) *fyne.Container {
	return container.NewHBox(
		layout.NewSpacer(),
		obj,
		layout.NewSpacer(),
	)
}

func alignRight(obj fyne.CanvasObject) *fyne.Container {
	return container.NewHBox(
		layout.NewSpacer(),
		obj,
	)
}

func NewBlankSession(app fyne.App, win fyne.Window, text string) *Session {
	return &Session{
		Content: container.NewCenter(widget.NewLabel(text)),
		app:     app,
		win:     win,
	}
}

func NewSession(app fyne.App, win fyne.Window, scenarioID int) (*Session, error) {
	base, err := backend.NewBaseFromApp(app)
	if err != nil {
		return nil, err
	}

	s := &Session{
		MsgEntry: widget.NewEntry(),
		Msgs:     make([]fyne.CanvasObject, 0),

		app: app,
		win: win,

		Base: base,
	}

	s.MsgEntry.PlaceHolder = "Send a message..."

	s.Conv, err = s.Base.Conv(scenarioID)
	if err != nil {
		return nil, err
	}

	s.MsgsCont = container.NewVBox(
		s.Msgs...,
	)

	s.MsgSendBtn = widget.NewButtonWithIcon(
		"",
		theme.MailSendIcon(),
		func() {
			s.SendFromEntry()
			s.MsgEntry.SetText("")
		},
	)

	s.MsgsReloadBtn = widget.NewButtonWithIcon(
		"",
		theme.ViewRefreshIcon(),
		func() {
			s.ReloadMsgs() // bad idea (data race), but who cares
		},
	)

	log, err := s.Conv.Log()
	var toolbarLogLen widget.ToolbarItem
	if err == nil {
		toolbarLogLen = newToolbarLabel("%v Log(s)", len(log))
	}

	s.Content = container.NewBorder(
		nil,
		container.NewVBox(
			s.MsgEntry,
			widget.NewToolbar(
				widget.NewToolbarAction(theme.ViewRefreshIcon(), s.ReloadMsgs),
				newToolbarLabel("Conversation ID: %v", s.Conv.ConversationID()),
				newToolbarLabel("Scenario ID: %v", s.Conv.Scenario().ID),
				newToolbarLabel("Scenario Level: %v", s.Conv.Scenario().Level),
				toolbarLogLen,
				widget.NewToolbarSpacer(),
				widget.NewToolbarAction(theme.ContentClearIcon(), func() {
					s.MsgEntry.SetText("")
				}),
				widget.NewToolbarAction(theme.MailSendIcon(), s.SendFromEntry),
			),
		),
		//container.NewHBox(
		//	s.MsgsReloadBtn,
		//	s.MsgEntry,
		//	s.MsgSendBtn,
		//),
		nil,
		nil,
		container.NewVScroll(s.MsgsCont),
	)

	s.ReloadMsgs()
	return s, nil
}

func newToolbarLabel(format string, a ...interface{}) widget.ToolbarItem {
	return WrappedToolbarItem{CanvasObject: widget.NewLabel(fmt.Sprintf(
		format,
		a...,
	))}
}

func (s *Session) ComplainIf(err error) {
	if unwrappedErr := xerrors.Unwrap(err); unwrappedErr != nil {
		s.Complain(unwrappedErr)
	}
	if err != nil {
		s.Complain(err)
	}
}

func (s *Session) Complain(err error) {
	fyne.LogError("complained", err)
	dialog.NewError(err, s.win).Show()
}

func (s *Session) SendFromEntry() {
	start := time.Now()
	defer log.Printf("Sent msg from entry in %v.", time.Since(start))
	s.MsgEntry.Disable()
	s.MsgSendBtn.Disable()
	defer s.MsgEntry.Enable()
	defer s.MsgSendBtn.Enable()
	err := s.Conv.Send(s.MsgEntry.Text)
	s.ComplainIf(err)
	s.ReloadMsgs() // bad idea (data race), but who cares
}

func (s *Session) ReloadMsgs() {
	start := time.Now()
	s.MsgsReloadBtn.Disable()
	defer log.Printf("Reloaded msgs in %v.", time.Since(start))
	defer s.MsgsReloadBtn.Enable()
	logItems, err := s.Conv.Log()
	if err != nil {
		s.ComplainIf(xerrors.Errorf("while reloading msgs: %w", err))
	}
	logObjs := make([]fyne.CanvasObject, len(logItems)+1)
	logObjs[0] = alignMiddle(widget.NewLabel(fmt.Sprintf(
		"Scenario ID: %v",
		s.Conv.Scenario().ID,
	)))
	for i, logItem := range logItems {
		align := alignMiddle
		var nameText string
		switch logItem.Type {
		case types.LogTypeInitial, types.LogTypeNarration:
			align = alignMiddle // left here so it is easier to read code IMO
			nameText = ""
		case types.LogTypeAI:
			align = alignLeft
			nameText = logItem.Name
		case types.LogTypeHuman:
			align = alignRight
			nameText = s.app.Preferences().StringWithFallback(consts.PrefSessionName, logItem.Name)
		}
		switch logItem.TextStatus {
		//case "unsafe_or_sensitive":
		//	logObjs[i+1] = align(widget.NewCard(
		//		nameText,
		//		"",
		//		widget.NewLabel(
		//			"The response included content deemed as sensitive or unsafe, so it was hidden.",
		//		),
		//	))
		default:
			logObjs[i+1] = align(widget.NewCard(
				nameText,
				"",
				widget.NewLabel(
					logItem.Text,
				),
			))
		}
	}
	s.MsgsCont.Objects = logObjs
}
