package ui

import (
	"io"
	"time"

	"github.com/faiface/beep"

	"github.com/faiface/beep/speaker"

	"github.com/faiface/beep/flac"
)

func PlayAudio(r io.Reader) (<-chan struct{}, error) {
	s, f, err := flac.Decode(r)
	if err != nil {
		return nil, err
	}
	defer func() { _ = s.Close() }()
	err = speaker.Init(f.SampleRate, f.SampleRate.N(100*time.Millisecond))
	if err != nil {
		return nil, err
	}
	defer speaker.Close()

	var done = make(chan struct{})
	speaker.Play(beep.Seq(s, beep.Callback(func() {
		done <- struct{}{}
	})))
	return done, nil
}
