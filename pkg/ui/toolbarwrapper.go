package ui

import "fyne.io/fyne/v2"

type WrappedToolbarItem struct {
	CanvasObject fyne.CanvasObject
}

func (w WrappedToolbarItem) ToolbarObject() fyne.CanvasObject {
	return w.CanvasObject
}
