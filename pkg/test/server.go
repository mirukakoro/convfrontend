package test

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/colourdelete/convfrontend/pkg/types"
)

type Server struct {
	Scenarios map[int]types.Scenario
	Addr      []string
}

func (s Server) Run() error {
	router := gin.Default()
	return router.Run(s.Addr...)
}

func (s Server) GoRun(errChan chan error) {
	errChan <- s.Run()
}
