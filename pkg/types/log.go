package types

const (
	// Might as well not use iota since I am unsure if LogTypeNarration and the others would be of type LogType
	LogTypeInitial   LogType = 1
	LogTypeNarration LogType = 2
	LogTypeAI        LogType = 3
	LogTypeHuman     LogType = 4
)

type LogType uint8

type LogItem struct {
	ID           int    `json:"id"`
	Conversation int    `json:"conversation"`
	Name         string `json:"name"`
	TextStatus   string
	Text         string  `json:"text"`
	Visible      bool    `json:"visible"`
	Editable     bool    `json:"editable"`
	Type         LogType `json:"type"`
	LogNumber    int     `json:"log_number"`
}
