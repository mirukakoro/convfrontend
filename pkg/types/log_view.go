package types

import (
	"bytes"
	"encoding/json"
	"errors"
	"golang.org/x/xerrors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

var LogViewRefURL *url.URL

func init() {
	var err error
	LogViewRefURL, err = url.Parse("/demo/conversation/log/view")
	if err != nil {
		panic(err)
	}
}

// LogView
func LogViewURL(baseURL *url.URL) *url.URL {
	return baseURL.ResolveReference(LogViewRefURL)
}

func LogView(baseURL *url.URL, client http.Client, password string, conversationID int) (LogViewResp, error) {
	req := LogViewReq{
		ConversationID: conversationID,
		Password:       password,
	}
	reqB, err := json.Marshal(req)
	if err != nil {
		return LogViewResp{}, err
	}
	reqBuf := bytes.NewBuffer(reqB)
	resp, err := client.Post(
		LogViewURL(baseURL).String(),
		"application/json",
		reqBuf,
	)
	if err != nil {
		return LogViewResp{}, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return LogViewResp{}, err
	}
	switch resp.StatusCode {
	case http.StatusOK:
		items := LogViewResp{}
		err = json.Unmarshal(b, &items)
		for i := range items {
			splitted := strings.SplitN(items[i].Text, "\n", 2)
			if len(splitted) == 1 {
				items[i].Text = splitted[0]
			} else {
				items[i].TextStatus = splitted[0]
				items[i].Text = splitted[1]
			}
		}
		if err != nil {
			return LogViewResp{}, xerrors.Errorf(
				"while unmarshalling JSON to get logs: %w",
				err,
			)
		}
		return items, nil
	case http.StatusForbidden:
		return LogViewResp{}, errors.New("incorrect password provided")
	case http.StatusInternalServerError:
		return LogViewResp{}, errors.New("internal server error")
	case http.StatusNotFound:
		return LogViewResp{}, errors.New("conversation ID nonexistent")
	default:
		return LogViewResp{}, xerrors.Errorf("non 200 status code (code %v): %s", resp.StatusCode, resp.Status)
	}
}

type LogViewReq struct {
	ConversationID int    `json:"conversation_id"`
	Password       string `json:"password"`
}

type LogViewResp []LogItem
