package types

import "net/url"

var LogEditRefURL *url.URL

func init() {
	var err error
	LogEditRefURL, err = url.Parse("/demo/conversation/log/edit/")
	if err != nil {
		panic(err)
	}
}

// LogEdit

func LogEditURL(baseURL *url.URL) *url.URL {
	return baseURL.ResolveReference(LogEditRefURL)
}

type LogEditReq struct{}

type LogEditResp struct{}
