package types

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

var ChatRefURL *url.URL

func init() {
	var err error
	ChatRefURL, err = url.Parse("/demo/conversation/chat/")
	if err != nil {
		panic(err)
	}
}

// Send
func ChatURL(baseURL *url.URL) *url.URL {
	return baseURL.ResolveReference(ChatRefURL)
}

func Chat(baseURL *url.URL, client http.Client, password string, conversationID int, userInput string) (ChatResp, error) {
	req := ChatReq{
		ConversationID: conversationID,
		UserInput:      userInput,
		Password:       password,
	}
	reqB, err := json.Marshal(req)
	if err != nil {
		return ChatResp{}, err
	}
	reqBuf := bytes.NewBuffer(reqB)
	resp, err := client.Post(
		ChatURL(baseURL).String(),
		"application/json",
		reqBuf,
	)
	if err != nil {
		return ChatResp{}, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ChatResp{}, err
	}
	marshalled := ChatResp{}
	err = json.Unmarshal(b, &marshalled)
	if err != nil {
		return ChatResp{}, err
	}
	return marshalled, nil
}

type ChatReq struct {
	ConversationID int    `json:"conversation_id"`
	UserInput      string `json:"user_input"`
	Password       string `json:"password"`
}

type ChatResp struct {
	Response LogItem `json:"response"`
}
