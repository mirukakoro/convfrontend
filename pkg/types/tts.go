package types

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"

	"golang.org/x/xerrors"
)

var TTSRefURL *url.URL

func init() {
	var err error
	TTSRefURL, err = url.Parse("/demo/conversation/log/view")
	if err != nil {
		panic(err)
	}
}

// TTS
func TTSURL(baseURL *url.URL) *url.URL {
	return baseURL.ResolveReference(TTSRefURL)
}

func TTS(baseURL *url.URL, client http.Client, password string, text string) (TTSResp, error) {
	req := TTSReq{
		Text:     text,
		Password: password,
	}
	reqB, err := json.Marshal(req)
	if err != nil {
		return TTSResp{}, err
	}
	reqBuf := bytes.NewBuffer(reqB)
	reqRaw, err := http.NewRequest(
		"GET",
		TTSURL(baseURL).String(),
		reqBuf,
	)
	if err != nil {
		return TTSResp{}, err
	}
	reqRaw.Header.Set("Content-Type", "application/json")
	respRaw, err := client.Do(reqRaw)
	if err != nil {
		return TTSResp{}, err
	}
	defer respRaw.Body.Close()
	b, err := ioutil.ReadAll(respRaw.Body)
	if err != nil {
		return TTSResp{}, err
	}
	switch respRaw.StatusCode {
	case http.StatusOK:
		return b, nil
	case http.StatusForbidden:
		return b, errors.New("incorrect password provided")
	case http.StatusInternalServerError:
		return b, errors.New("internal server error")
	case http.StatusNotFound:
		return b, errors.New("conversation ID nonexistent")
	default:
		return b, xerrors.Errorf("non 200 status code (code %v): %s", respRaw.StatusCode, respRaw.Status)
	}
}

type TTSReq struct {
	Text     string `json:"text"`
	Password string `json:"password"`
}

type TTSResp []byte
