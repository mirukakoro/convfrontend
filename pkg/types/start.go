package types

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"golang.org/x/xerrors"
	"io/ioutil"
	"net/http"
	"net/url"
)

var StartRefURL *url.URL

func init() {
	var err error
	StartRefURL, err = url.Parse("/demo/conversation/")
	if err != nil {
		panic(err)
	}
}

// Start

func StartURL(baseURL *url.URL) *url.URL {
	return baseURL.ResolveReference(StartRefURL)
}

func Start(baseURL *url.URL, client http.Client, scenarioID int, password string) (resp StartResp, err error) {
	req := StartReq{
		ScenarioID: scenarioID,
		Password:   password,
	}
	reqB, err := json.Marshal(req)
	if err != nil {
		return StartResp{}, err
	}
	reqBuf := bytes.NewBuffer(reqB)
	respRaw, err := client.Post(
		StartURL(baseURL).String(),
		"application/json",
		reqBuf,
	)
	defer respRaw.Body.Close()
	switch err {
	case nil:
		break
	case context.DeadlineExceeded:
		return StartResp{}, errors.New("http request timed out")
	default:
		return StartResp{}, err
	}
	b, err := ioutil.ReadAll(respRaw.Body)
	if err != nil {
		return StartResp{}, err
	}
	switch respRaw.StatusCode {
	case http.StatusOK:
		err = json.Unmarshal(b, &resp)
		if err != nil {
			return StartResp{Raw: b}, xerrors.Errorf(
				"JSON unmarshal failed: %w",
				err,
			)
		}
		resp.Raw = b
		return resp, nil
	case http.StatusForbidden:
		return StartResp{Raw: b}, errors.New("incorrect password provided")
	case http.StatusInternalServerError:
		return StartResp{Raw: b}, errors.New("internal server error")
	case http.StatusNotFound:
		return StartResp{Raw: b}, errors.New("conversation ID nonexistent")
	default:
		return StartResp{Raw: b}, xerrors.Errorf("non 200 status code (%v): %s", respRaw.StatusCode, respRaw.Status)
	}
}

type StartReq struct {
	ScenarioID int    `json:"scenario_id"`
	Password   string `json:"password"`
}

type StartResp struct {
	ConversationID int      `json:"conversation_id"`
	ScenarioData   Scenario `json:"scenario_data"`
	Raw            []byte
}
