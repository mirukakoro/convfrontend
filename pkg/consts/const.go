package consts

const (
	AppID                       = "io.cdel.aibou.client"
	PrefSessionName             = "session-name"
	PrefSessionAPIPassword      = "session-api-password"
	PrefSessionAPIClientTimeout = "session-api-client-timeout"
)
