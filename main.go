package main

import "fmt"

func main() {
	fmt.Println(`# gitlab.com/colourdelete/convfrontend aka AIbou Client

This file (main.go) is a placeholder.
Do not use this in production or as a program.
This doesn't do anything too useful!

You probably want to use these:
- Main App: gitlab.com/colourdelete/convfrontend/cmd/convfrontend
- Packages: gitlab.com/colourdelete/convfrotnend/pkg/...`)
}
