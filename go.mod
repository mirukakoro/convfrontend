module gitlab.com/colourdelete/convfrontend

go 1.16

require (
	fyne.io/fyne v1.4.3 // indirect
	fyne.io/fyne/v2 v2.0.0
	github.com/faiface/beep v1.0.2
	github.com/gin-gonic/gin v1.6.3
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543
)
